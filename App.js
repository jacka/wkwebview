import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import WebView from 'react-native-webview';
import WkWebView from 'react-native-wkwebview-reborn';

const htmlContent = `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}

.field_label {
	font-size: 14px;
	font-weight: bold;
	color: #FFF;
	background-color: #903;
}
.field_input {
	background-color: #EEE;
}
.bt_submit {
	background-color: #CCC;
	border: 2px solid #F30;
	color: #900;
	padding-right: 20px;
	padding-left: 20px;
	padding-top: 5px;
	padding-bottom: 5px;
	font-weight: bold;
}

#menu dt {
	float:left;
}
#menu dt a {
	display:block;
	padding:10px;
	margin:0 1px 0 1px;
	background-color:#CCC;
	text-decoration:none;
	color:#000;
	font-weight:bold;
}
#menu dt a:hover {
	background-color:#09C;
}
</style>
        <script language="javascript" type="text/javascript">

function autoSubmit(){
  document.getElementById('submit').click();
}
window.onload = autoSubmit;



</script>
    </head>
    <body style="display: none;">
        <div style="clear:both"></div>
        <form action="https://staging.doku.com/Suite/Receive" id="MerchatPaymentPage" name="MerchatPaymentPage" method="post" >
            <table width="600" border="0" cellspacing="1" cellpadding="5">
                <tr>
                    <td width="100" class="field_label">BASKET</td>
                    <td width="500" class="field_input">
                        <input name="BASKET" type="text" id="BASKET" value="Item 1,10000.00,1,10000.00" size="100" />
                    </td>
                </tr>
                <tr>
                    <td width="100" class="field_label">MALLID</td>
                    <td width="500" class="field_input"><input name="MALLID" type="text" id="MALLID" value="7025" size="12" /> --> Mall ID OCO
                    </td>
                </tr>
                <tr>
                    <td width="100" class="field_label">CHAINMERCHANT</td>
                    <td width="500" class="field_input">
                        <input name="CHAINMERCHANT" type="text" id="CHAINMERCHANT" value="NA" size="12" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">CURRENCY</td>
                    <td class="field_input">
                        <input name="CURRENCY" type="text" id="CURRENCY" value="360" size="3" maxlength="3" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">PURCHASECURRENCY</td>
                    <td class="field_input">
                        <input name="PURCHASECURRENCY" type="text" id="PURCHASECURRENCY" value="360" size="3" maxlength="3" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">AMOUNT</td>
                    <td class="field_input">
                        <input name="AMOUNT" type="text" id="AMOUNT" value="570000.00" size="12" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">PURCHASEAMOUNT</td>
                    <td class="field_input">
                        <input name="PURCHASEAMOUNT" type="text" id="PURCHASEAMOUNT" value="570000.00" size="12" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">TRANSIDMERCHANT</td>
                    <td class="field_input">
                        <input name="TRANSIDMERCHANT" value="14364D10420" type="text" id="TRANSIDMERCHANT" size="16" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">SHAREDKEY</td>
                    <td class="field_input"><input name="SHAREDKEY" type="text" id="SHAREDKEY" value="G7K7zUjUb8w9" size="15" maxlength="12"/> --> Shared Key OCO
                    </td>
                </tr>
                <tr>
                    <td class="field_label">WORDS</td>
                    <td class="field_input">
                        <input type="text" value="ad6ec5d55bc978ef9c2080f440d7f5bfbf6f86f5" name="WORDS"  size="60" />&nbsp;&nbsp;
                        <input type="button" value="Generate WORDS" onClick="getWords();">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="field_label">REQUESTDATETIME</td>
                    <td class="field_input">
                        <input name="REQUESTDATETIME" type="text" value="20200419140436" size="14" maxlength="14" />
      (YYYYMMDDHHMMSS)
                    </td>
                </tr>
                <tr>
                    <td class="field_label">SESSIONID</td>
                    <td class="field_input">
                        <input type="text" value="B4ZcEbOoLUMdKXwGZ2C2OG6Qm6TfDpKdVzb40Lc1HFjInDGg" name="SESSIONID" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">PAYMENTCHANNEL</td>
                    <td class="field_input"><input type="text" id="PAYMENTCHANNEL" name="PAYMENTCHANNEL" value="" /> --> kosongkan jika ingin menampilkan semua payment channel yang digunakan
                    </td>
                </tr>
                <tr>
                    <td width="100" class="field_label">EMAIL</td>
                    <td width="500" class="field_input">
                        <input name="EMAIL" type="text" id="EMAIL" value="orari.kta@gmail.com" size="12" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">NAME</td>
                    <td class="field_input">
                        <input name="NAME" type="text" id="NAME" value="EKO SETYAWAN ES" size="30" maxlength="50" />
                    </td>
                </tr>
                <tr>
                    <td class="field_label">MOBILEPHONE</td>
                    <td class="field_input">
                        <input name="MOBILEPHONE" type="text" id="MOBILEPHONE" value="" size="12" maxlength="20" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <input name="submit" type="submit" class="bt_submit" id="submit" value="SUBMIT" />
        </form>
    </body>
</html>
`;

export default class example extends Component {
    render() {
        return (
              <View style={{ flex: 1 }}>
                <WkWebView style={{ backgroundColor: '#fff' }}
                  automaticallyAdjustContentInsets={false}
                  style={{ flex: 1, marginTop: 40 }}
                  hideKeyboardAccessoryView={false}
                  ref={(c) => this.webview = c}
                  sendCookies={true}
                //   source={{ html: htmlContent }}
                  source={{ uri: `http://www.pmi.or.id/` }}
                  onMessage={(e) => console.log(e.nativeEvent)}
                  onNavigationResponse={(e) => console.log(e.nativeEvent)}
                />
                  <Text style={{ fontWeight: 'bold', padding: 10 }} onPress={() => this.webview.reload()}>Reload</Text> 
                  <Text style={{ fontWeight: 'bold', padding: 10 }} onPress={() => this.webview.postMessage("Hello from React Native")}>Post Message</Text>
              </View>
        );
    }
}
